#!/bin/bash
VERSION=$1
CODENAME=lory

function _mount {
    mount -t proc proc $1/proc/
    mount -t sysfs sysfs $1/sys/
    mount -t devtmpfs dev $1/dev/
    mkdir -p $1/dev/pts/
    mount -t devpts devpts $1/dev/pts/
    mount -t tmpfs tmpfs $1/run/
}

function _umount {
    umount -df $1/dev/pts || true
    umount -df $1/dev
    umount -df $1/proc/sys/fs/binfmt_misc || true
    umount -df $1/proc/
    umount -df $1/sys/
    umount -df $1/run/
}

function bootstrap {
	ARCH=$1
	EDITION=$2
	CODENAME=$3

	echo "Building $EDITION-$ARCH"

	rm -r $EDITION-$ARCH/ || true
	debootstrap --arch=$ARCH --components=main,contrib,non-free,non-free-firmware --include=gnupg2,nano,base-files,qemu-user-static --exclude=parrot-core,debian-archive-keyring $CODENAME $EDITION-$ARCH https://deb.parrot.sh/direct/parrot/
	
	echo "Customizing $EDITION-$ARCH"

	_mount $EDITION-$ARCH
    chroot $EDITION-$ARCH bash -c "apt update && apt -y install parrot-core"
    _umount $EDITION-$ARCH

    rm -rf $EDITION-$ARCH/var/cache/apt/* $EDITION-$ARCH/var/lib/apt/lists/*
    find $EDITION-$ARCH -name "*__pycache__*" -exec rm -rf {} \;

	echo "Done $EDITION-$ARCH"
}

function configure_home {
	ARCH=$1
	EDITION=$2

	_mount $EDITION-$ARCH
    chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt update && apt -y install xserver-xorg-input-all xserver-xorg-video-all"
	chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt -y install linux-image-$ARCH linux-headers-$ARCH grub-efi-$ARCH"
	chroot $EDITION-$ARCH bash -c "echo | DEBIAN_FRONTEND=noninteractive apt -y install locales locales-all plymouth plymouth-themes tasksel cryptsetup cryptsetup-nuke-password iw lvm2 mdadm parted gpart bash-completion rng-tools5 haveged"
	chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt -y install neovim inxi neofetch htop nload iftop jfsutils hfsplus hfsutils btrfs-progs e2fsprogs dosfstools mtools reiser4progs reiserfsprogs xfsprogs xfsdump ntfs-3g libfsapfs1 libfsapfs-utils apparmor apparmor-profiles apparmor-profiles-extra apparmor-utils apparmor-easyprof apparmor-notify"
	chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt -y install qemu-guest-agent spice-vdagent open-vm-tools-desktop"
	chroot $EDITION-$ARCH bash -c "apt -y install virtualbox-guest-x11 parrot-meta-privacy || true"
	chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt -y install parrot-meta-devel parrot-meta-crypto parrot-interface parrot-interface-home parrot-desktop-mate codium golang-go nodejs npm default-jdk python3-pip git podman podman-docker flatpak flatpak-xdg-utils chromium- mate-user-guide- pocketsphinx-en-us- libreoffice-help-en-us- mythes-en-us- libreoffice-help-common- espeak-ng-data-"
	chroot $EDITION-$ARCH bash -c "/usr/share/parrot-menu/update-launchers"
	chroot $EDITION-$ARCH bash -c "cp /etc/lightdm/slick-greeter-$EDITION.conf /etc/lightdm/slick-greeter.conf || true"
	chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt update && apt -y full-upgrade -t lory-backports"
	chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt -y autoremove --purge && apt clean"
	chroot $EDITION-$ARCH bash -c "systemctl disable postgres || true; systemctl disable gsad || true; systemctl disable redis-server || true; systemctl disable xrdp || true; systemctl disable opensnitch || true"
	chroot $EDITION-$ARCH bash -c "flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo"
	chroot $EDITION-$ARCH bash -c "update-initramfs -u"
	chroot $EDITION-$ARCH bash -c "useradd -m -s /bin/bash -p \"$(openssl passwd parrot)\" user"
	chroot $EDITION-$ARCH bash -c "for group in dialout cdrom floppy sudo audio dip video plugdev netdev bluetooth lpadmin scanner docker; do adduser user $group || true; done"
	chroot $EDITION-$ARCH bash -c "sed -i \"s/#autologin-user=/autologin-user=user/\" /etc/lightdm/lightdm.conf"
	chroot $EDITION-$ARCH bash -c "echo \"user ALL=(ALL) NOPASSWD: ALL\" > /etc/sudoers.d/20-parrot-virtual"
	rm $EDITION-$ARCH/root/.bash_history
    _umount $EDITION-$ARCH	
}

function configure_security {
    ARCH=$1
    EDITION=$2

    _mount $EDITION-$ARCH
    chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt -y install parrot-tools-full"
    chroot $EDITION-$ARCH bash -c "cp /etc/lightdm/slick-greeter-security.conf /etc/lightdm/slick-greeter.conf || true"
    chroot $EDITION-$ARCH bash -c "DEBIAN_FRONTEND=noninteractive apt -y autoremove --purge && apt clean"
    chroot $EDITION-$ARCH bash -c "systemctl disable postgres || true; systemctl disable gsad || true; systemctl disable redis-server || true; systemctl disable xrdp || true; systemctl disable opensnitch || true"
    rm $EDITION-$ARCH/root/.bash_history
    _umount $EDITION-$ARCH
}
function image {
	ARCH=$1
	EDITION=$2
	VERSION=$3
	TEMP=$(mktemp -d)
	echo "Assembling virtual image"
	qemu-img create -f qcow2 Parrot-$EDITION-$VERSION_$ARCH.qcow2 128G
	modprobe nbd max_part=4
	qemu-nbd --connect /dev/nbd0 Parrot-$EDITION-$VERSION_$ARCH.qcow2
	echo -e "g
n
1

+50M
t
1
n
2


p
w" | fdisk /dev/nbd0
    mkfs.fat -F 32 /dev/nbd0p1
    mkfs.btrfs /dev/nbd0p2
    mount /dev/nbd0p2 $TEMP
    btrfs subvol create $TEMP/@
    btrfs subvol create $TEMP/@home
    umount $TEMP
    mount -o subvol=@,defaults,autodefrag,compress=zstd /dev/nbd0p2 $TEMP
    mount -o subvol=@home,defaults,autodefrag,compress=zstd /dev/nbd0p2 $TEMP/home
    mkdir -p $TEMP/boot/efi
    mount /dev/nbd0p1 $TEMP/boot/efi
    rsync -Pah home-$ARCH/ $TEMP/
    _mount $TEMP
    chroot $TEMP bash -c "grub-install"
    chroot $TEMP bash -c "update-initramfs -u"
    chroot $TEMP bash -c "update-grub"
    echo $(blkid | grep nbd0p1 | cut -d' ' -f2) /boot/efi vfat defaults,noatime,nodiratime > $TEMP/etc/fstab
    echo $(blkid | grep nbd0p2 | cut -d' ' -f2) / btrfs subvol=@,defaults,noatime,nodiratime,autodefrag,compress=zstd >> $TEMP/etc/fstab
    echo $(blkid | grep nbd0p2 | cut -d' ' -f2) /home btrfs subvol=@home,defaults,noatime,nodiratime,autodefrag,compress=zstd >> $TEMP/etc/fstab
    _umount $TEMP
    umount $TEMP/boot/efi
    umount $TEMP/home
    umount $TEMP
    qemu-nbd --disconnect /dev/nbd0
}

#mkdir -p images
for ARCH in amd64 arm64; do
   	bootstrap $ARCH home $CODENAME
   	configure_home $ARCH home $CODENAME
   	image $ARCH home $VERSION
   	mv Parrot-home* images/
   	configure_security $ARCH home $VERSION
   	image $ARCH security $VERSION
   	mv Parrot-security* images/
#   	rm -rf $EDITION-$ARCH
done
